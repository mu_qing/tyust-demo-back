package com.example.mybatisplus.service;

import com.example.mybatisplus.model.domain.SysAdmin;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lxq
 * @since 2023-01-02
 */
public interface SysAdminService extends IService<SysAdmin> {

    SysAdmin login(SysAdmin sysAdmin);

}
